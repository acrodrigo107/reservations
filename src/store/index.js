import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    reserveData: [],
    timeLine: {},
    container: '',
    option: {}
  },
  getters:{
    getReserveData: state  => state.reserveData,
    getTimeLine: state  => state.timeLine,
    getContainer: state  => state.container,
    getOptions: state  => state.option,
  },
  mutations: {
    addReserveData(state, data){
      let reservers = state.reserveData;
      reservers.push(data);
      state.reserveData = reservers;
    },
    setTimeLine(state, data){
      state.timeLine = data;
    },
    setContainer(state, data){
      state.container = data;
    },
    setOptions(state, data){
      state.option = data;
    },
    subReserveData(state, data){
      state.reserveData.splice(data, 1);
    }
  },
  actions: {

  },
  modules: {
  }
})
